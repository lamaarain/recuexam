import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import exam.exam2;

@RunWith(Parameterized.class)
public class examParameters {

	int value;
	boolean res;
	
	public examParameters(int value, boolean res) {
		this.value = value;
		this.res = res;
	}
	
	@Parameters
	public static Collection<Object[]> numbers() {
		return Arrays.asList(new Object[][] {
			{0, true},
			{1, true},
			{2, false},
			{5, false},
			{9, true},
		});
	}
	
	@Test
	public void test() {
		assertEquals(exam2.exam(value), res);
	}

}
